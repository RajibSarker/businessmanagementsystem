﻿using BusinessManagementSystemApp.Models.Models;
using BusinessManagementSystemApp.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessManagementSystemApp.BLL.Manager
{
    public class StockOutManager
    {
        StockOutRepository _stockOutRepository = new StockOutRepository();
        public bool SaveStockOutProduct(StockOut stockOut)
        {
            return _stockOutRepository.SaveStockOutProduct(stockOut);
        }
    }
}
