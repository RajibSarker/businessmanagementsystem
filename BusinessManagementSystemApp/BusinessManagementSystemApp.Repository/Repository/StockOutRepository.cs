﻿using BusinessManagementSystemApp.DatabaseContext.DatabaseContext;
using BusinessManagementSystemApp.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessManagementSystemApp.Repository.Repository
{
    public class StockOutRepository
    {
        BusinessManagementDbContext db = new BusinessManagementDbContext();
        public bool SaveStockOutProduct(StockOut stockOut)
        {
            db.StockOuts.Add(stockOut);
            return db.SaveChanges() > 0;
        }
    }
}
