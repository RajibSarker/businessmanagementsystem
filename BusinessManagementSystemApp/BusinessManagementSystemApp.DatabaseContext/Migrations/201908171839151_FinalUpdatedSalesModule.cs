namespace BusinessManagementSystemApp.DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FinalUpdatedSalesModule : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sales", "PayableAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SalesDetails", "UnitPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SalesDetails", "TotalPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.SalesDetails", "PayableAmount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SalesDetails", "PayableAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.SalesDetails", "TotalPrice");
            DropColumn("dbo.SalesDetails", "UnitPrice");
            DropColumn("dbo.Sales", "PayableAmount");
        }
    }
}
