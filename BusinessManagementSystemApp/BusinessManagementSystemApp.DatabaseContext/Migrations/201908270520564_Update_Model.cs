namespace BusinessManagementSystemApp.DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Model : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Suppliers", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Suppliers", "Date", c => c.String());
        }
    }
}
