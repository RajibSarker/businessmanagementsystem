namespace BusinessManagementSystemApp.DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateStockOut : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StockOuts", "TotalPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StockOuts", "TotalPrice");
        }
    }
}
