namespace BusinessManagementSystemApp.DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateStockOut1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.StockOuts", "OutType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.StockOuts", "OutType", c => c.String());
        }
    }
}
