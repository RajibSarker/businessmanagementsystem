namespace BusinessManagementSystemApp.DatabaseContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FinalUpdatedSalesModule2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sales", "DiscountPerCent", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Sales", "DiscountAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sales", "DiscountAmount");
            DropColumn("dbo.Sales", "DiscountPerCent");
        }
    }
}
